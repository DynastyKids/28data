<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Btc Entity
 *
 * @property int $draw
 * @property \Cake\I18n\FrozenTime|null $time
 * @property string|null $rawresult
 * @property string|null $calc
 * @property int|null $result
 * @property string|null $pred_calc
 * @property int|null $pred_result
 */
class Btc extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'time' => true,
        'rawresult' => true,
        'calc' => true,
        'result' => true,
        'pred_calc' => true,
        'pred_result' => true,
    ];
}
