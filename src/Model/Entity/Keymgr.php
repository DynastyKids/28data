<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Keymgr Entity
 *
 * @property int $id
 * @property string $datakey
 * @property \Cake\I18n\FrozenTime $expire
 * @property int $28ca
 * @property int $28tw
 * @property int $28btc
 */
class Keymgr extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'datakey' => true,
        'expire' => true,
        '28ca' => true,
        '28tw' => true,
        '28btc' => true,
    ];
}
