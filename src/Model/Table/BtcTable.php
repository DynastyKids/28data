<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Btc Model
 *
 * @method \App\Model\Entity\Btc get($primaryKey, $options = [])
 * @method \App\Model\Entity\Btc newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Btc[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Btc|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Btc saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Btc patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Btc[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Btc findOrCreate($search, callable $callback = null, $options = [])
 */
class BtcTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('btc');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('draw', null, 'create')
            ->add('draw', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->dateTime('time')
            ->allowEmptyDateTime('time');

        $validator
            ->scalar('rawresult')
            ->maxLength('rawresult', 16777215)
            ->allowEmptyString('rawresult');

        $validator
            ->scalar('calc')
            ->maxLength('calc', 16777215)
            ->allowEmptyString('calc');

        $validator
            ->integer('result')
            ->allowEmptyString('result');

        $validator
            ->scalar('pred_calc')
            ->maxLength('pred_calc', 45)
            ->allowEmptyString('pred_calc');

        $validator
            ->integer('pred_result')
            ->allowEmptyString('pred_result');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['draw']));

        return $rules;
    }
}
