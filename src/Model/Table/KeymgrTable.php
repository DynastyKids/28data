<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Keymgr Model
 *
 * @method \App\Model\Entity\Keymgr get($primaryKey, $options = [])
 * @method \App\Model\Entity\Keymgr newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Keymgr[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Keymgr|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Keymgr saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Keymgr patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Keymgr[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Keymgr findOrCreate($search, callable $callback = null, $options = [])
 */
class KeymgrTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('keymgr');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('datakey')
            ->maxLength('datakey', 64)
            ->requirePresence('datakey', 'create')
            ->notEmptyString('datakey')
            ->add('datakey', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->dateTime('expire')
            ->notEmptyDateTime('expire');

        $validator
            ->integer('28ca')
            ->notEmptyString('28ca');

        $validator
            ->integer('28tw')
            ->notEmptyString('28tw');

        $validator
            ->integer('28btc')
            ->notEmptyString('28btc');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['datakey']));

        return $rules;
    }
}
