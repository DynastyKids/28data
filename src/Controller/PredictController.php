<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use http\Env\Response;

/**
 * Predict Controller
 *
 *
 * @method \App\Model\Entity\Predict[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PredictController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index($key=null)
    {
        $this->autoLayout = false;
        $this->render(false);
        $returnmsg = json_encode(["Status"=>"01","Msg"=>"Missing API Key"],true);
        if($key!=null) {
            $result = TableRegistry::getTableLocator()->get('keymgr')->find('all', ['conditions' => ['datakey' => $key]]);
            if ($result->count() > 0) {
                $returnmsg = json_encode(["Status"=>"11","Msg"=>"API Entrance Error"],true);
            }
        }
        $this->response->body($returnmsg);
        $this->response->statusCode(200);
        $this->response->type("application/json");
    }

    /**
     * bg method
     *
     * @return \Cake\Http\Response|null
     */
    public function bg($key = null)
    {
        $this->redirect(['controller'=>'Bg','action'=>'predict',$key]);
    }

    /**
     * bg method
     *
     * @return \Cake\Http\Response|null
     */
    public function btc($key = null)
    {
        $this->redirect(['controller'=>'Btc','action'=>'predict',$key]);
    }

    /**
     * bg method
     *
     * @return \Cake\Http\Response|null
     */
    public function ca($key = null)
    {
        $this->redirect(['controller'=>'Ca','action'=>'predict',$key]);
    }

    public function all($key=null){
        $this->autoLayout = false;
        $this->render(false);

        $size=100;
        switch (sizeof($this->request['pass'])) {
            case 1:
                $key = $this->request['pass'][0];
                break;
            case 2:
                $size = $key = $this->request['pass'][0];
                $key = $this->request['pass'][1];
                break;
        }
        if($size>100){
            $size=100;
        }

        if($key!=null){
            $result=TableRegistry::getTableLocator()->get('keymgr')->find('all',['conditions' => ['datakey' => $key]]);
            if($result->count()>0){
                if($result->toArray()[0]['28tw']==0 || $result->toArray()[0]['28btc']==0 || $result->toArray()[0]['28ca']==0){
                    $returnmsg = json_encode(["Status"=>"04", "Msg"=>"API Key privileges error"],true);
                } else {
                    $currentbg = TableRegistry::getTableLocator()->get('bg')->find('all')->whereNotNull('result')->limit(1)->orderDesc('draw');
                    $currentbtc = TableRegistry::getTableLocator()->get('btc')->find('all')->whereNotNull('result')->limit(1)->orderDesc('draw');
                    $currentca = TableRegistry::getTableLocator()->get('ca')->find('all')->whereNotNull('result')->limit(1)->orderDesc('draw');
                    if($currentbg->count()>0) {
                        $nextbg = $currentbg->toArray()[0]['draw'] + 1;
                        $predictbg = TableRegistry::getTableLocator()->get('bg')->find('all')->where(['draw<='.$nextbg])->orderDesc('draw')->limit($size);

                        $newbg = $predictbg->toArray();
                        for ($i=0; $i < sizeof($predictbg->toArray()); $i++) {
                            $newbg[$i]['size'] = 0;
                            if($predictbg->toArray()[$i]['result'] >=14){
                                $newbg[$i]['size'] = 1;
                            }
                            $newbg[$i]['odd'] = $predictbg->toArray()[$i]['result']%2;
                        }
                    }
                    if($currentbtc->count()>0) {
                        $nextbtc = $currentbtc->toArray()[0]['draw'] + 1;
                        $predictbtc = TableRegistry::getTableLocator()->get('btc')->find('all')->where(['draw<='.$nextbtc])->orderDesc('draw')->limit($size);
                        $newbtc = $predictbtc->toArray();
                            for ($i=0; $i < sizeof($predictbtc->toArray()); $i++) {
                                $newbtc[$i]['size'] = 0;
                                if($predictbtc->toArray()[$i]['result'] >=14){
                                    $newbtc[$i]['size'] = 1;
                                }
                                $newbtc[$i]['odd'] = $predictbtc->toArray()[$i]['result']%2;
                            }
                    }
                    if($currentca->count()>0) {
                        $nextca = $currentca->toArray()[0]['draw'] + 1;
                        $predictca = TableRegistry::getTableLocator()->get('ca')->find('all')->where(['draw<='.$nextca])->orderDesc('draw')->limit($size);
                        $newca = $predictca->toArray();
                            for ($i=0; $i < sizeof($predictca->toArray()); $i++) {
                                $newca[$i]['size'] = 0;
                                if($predictca->toArray()[$i]['result'] >=14){
                                    $newca[$i]['size'] = 1;
                                }
                                $newca[$i]['odd'] = $predictca->toArray()[$i]['result']%2;
                            }
                    }
                    $returnmsg = json_encode(["Status" => "00", "Msg" => "Approved", "Data" => ['Bg'=>$newbg,'Btc'=>$newbtc,'Ca'=>$newca]], true);
                }
                $time = new Time($result->toArray()[0]['expire']);
                if($time->isPast() == true) {
                    $returnmsg = json_encode(["Status"=>"03", "Msg"=>"API Key Has Expired"],true);
                }
            } else {
                $returnmsg = json_encode(["Status"=>"02", "Msg"=>"API Key is incorrect"],true);
            }
        } else {
            $returnmsg = json_encode(["Status"=>"01","Msg"=>"Missing API Key"],true);
        }

        $this->response->body($returnmsg);
        $this->response->statusCode(200);
        $this->response->type("application/json");
    }
}
