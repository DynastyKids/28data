<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * History Controller
 *
 * @method \App\Model\Entity\Predict[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HistoryController extends AppController
{
    /**
     * Index method
     *
     * @param string|null $key User's key.
     * @return \Cake\Http\Response|null
     */
    public function index($key=null){
        $this->autoLayout = false;
        $this->render(false);
        $returnmsg = '{"Status":"01","Msg":"Missing API Key"}';
        if($key!=null) {
            $result = TableRegistry::getTableLocator()->get('keymgr')->find('all', ['conditions' => ['datakey' => $key]]);
            if ($result->count() > 0) {
                $returnmsg = '{"Status":"01","Msg":"API Error"}';
            }
        }
        $this->response->body($returnmsg);
        $this->response->statusCode(200);
        $this->response->type("application/json");
    }

    /**
     * bg method
     *
     * @param string|null $key User's key.
     * @return \Cake\Http\Response|null
     */
    public function bg($key = null){
        $this->redirect(['controller'=>'Bg','action'=>'history',$key]);
    }

    /**
     * bg method
     *
     * @param string|null $key User's key.
     * @return \Cake\Http\Response|null
     */
    public function btc($key = null){
        $this->redirect(['controller'=>'Btc','action'=>'history',$key]);
    }

    /**
     * bg method
     *
     * @param string|null $key User's key.
     * @return \Cake\Http\Response|null
     */
    public function ca($key = null){
        $this->redirect(['controller'=>'Ca','action'=>'history',$key]);
    }

    public function all($key=null){
        $this->autoLayout = false;
        $this->render(false);
        $size=100;
        switch (sizeof($this->request['pass'])) {
            case 1:
                $key = $this->request['pass'][0];
                break;
            case 2:
                $size = $key = $this->request['pass'][0];
                $key = $this->request['pass'][1];
                break;
        }
        if($size>100){
            $size=100;
        }

        if($key!=null){
            $result=TableRegistry::getTableLocator()->get('keymgr')->find('all',['conditions' => ['datakey' => $key]]);
            if($result->count()>0){
                if($result->toArray()[0]['28tw']==0 || $result->toArray()[0]['28btc']==0 || $result->toArray()[0]['28ca']==0){
                    $returnmsg = json_encode(["Status"=>"04", "Msg"=>"API Key privileges error"],true);
                } else {
                    // 加载历史数据，MYSQL转JSON输出
                    $bgdata = TableRegistry::getTableLocator()->get('bg')->find('all')->whereNotNull('result')->orderDesc('draw')->limit($size);
                    $newbgdata = $bgdata->toArray();
                    for ($i=0; $i < sizeof($bgdata->toArray()); $i++) {
                        $newbgdata[$i]['size']=0;
                        if($bgdata->toArray()[$i]['result'] >= 14){
                            $newbgdata[$i]['size']=1;
                        }
                        $newbgdata[$i]['odd']=$bgdata->toArray()[$i]['result']%2;
                    }

                    $cadata = TableRegistry::getTableLocator()->get('ca')->find('all')->whereNotNull('result')->orderDesc('draw')->limit($size);
                    $newcadata = $cadata->toArray();
                    for ($i=0; $i < sizeof($cadata->toArray()); $i++) {
                        $newcadata[$i]['size']=0;
                        if($cadata->toArray()[$i]['result'] >= 14){
                            $newcadata[$i]['size']=1;
                        }
                        $newcadata[$i]['odd']=$cadata->toArray()[$i]['result']%2;
                    }

                    $btcdata = TableRegistry::getTableLocator()->get('btc')->find('all')->whereNotNull('result')->orderDesc('draw')->limit($size);
                    $newbtcdata = $btcdata->toArray();
                    for ($i=0; $i < sizeof($btcdata->toArray()); $i++) {
                        $newbtcdata[$i]['size']=0;
                        if($btcdata->toArray()[$i]['result'] >= 14){
                            $newbtcdata[$i]['size']=1;
                        }
                        $newbtcdata[$i]['odd']=$btcdata->toArray()[$i]['result']%2;
                    }


                    $returnmsg = json_encode(["Status"=>"00","Msg"=>"Approved","Data"=>['Bg'=>$newbgdata,'Btc'=>$newbtcdata,'Ca'=>$newcadata]],true);
                }
                $time = new Time($result->toArray()[0]['expire']);
                if($time->isPast() == true) {
                    $returnmsg = json_encode(["Status"=>"03", "Msg"=>"API Key Has Expired"],true);
                }
            } else {
                $returnmsg = json_encode(["Status"=>"02", "Msg"=>"API Key is incorrect"],true);
            }
        } else {
            $returnmsg = json_encode(["Status"=>"01","Msg"=>"Missing API Key"],true);
        }

        $this->response->body($returnmsg);
        $this->response->statusCode(200);
        $this->response->type("application/json");
    }
}
