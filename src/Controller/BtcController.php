<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * Btc Controller
 *
 * @property \App\Model\Table\BtcTable $Btc
 *
 * @method \App\Model\Entity\Btc[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BtcController extends AppController
{
    public function index($key=null)
    {
        $this->autoLayout = false;
        $this->render(false);
        $returnmsg = json_encode(["Status"=>"01","Msg"=>"Missing API Key"],true);
        if($key!=null) {
            $result = TableRegistry::getTableLocator()->get('keymgr')->find('all', ['conditions' => ['datakey' => $key]]);
            if ($result->count() > 0) {
                $returnmsg = json_encode(["Status"=>"11","Msg"=>"API Entrance Error"],true);
            }
        }
        $this->response->body($returnmsg);
        $this->response->statusCode(200);
        $this->response->type("application/json");
    }

    /**
     * Predict method
     *
     * @param string|null $key Keymgr's id.
     * @return \Cake\Http\Response|null
     */
    public function predict($key=null)
    {
        $this->autoLayout = false;
        $this->render(false);
        $size=100;
        switch (sizeof($this->request['pass'])) {
            case 1:
                $key = $this->request['pass'][0];
                break;
            case 2:
                $size = $this->request['pass'][0];
                $key = $this->request['pass'][1];
                break;
        }
        if($size>300){
            $size=300;
        }
        $returnmsg = json_encode(["Status"=>"01","Msg"=>"Missing API Key"],true);
        if($key!=null){
            $result=TableRegistry::getTableLocator()->get('keymgr')->find('all',['conditions' => ['datakey' => $key]]);
            if($result->count()>0){
                if($result->toArray()[0]['28btc']==0){
                    $returnmsg = json_encode(["Status"=>"04", "Msg"=>"API Key privileges error"],true);
                } else {
                    $currentdraw = $this->Btc->find('all')->whereNotNull('result')->limit(1)->orderDesc('draw');
                    $returnmsg = json_encode(["Status" => "00", "Msg" => "Approved", "Data" => ""], true);
                    if($currentdraw->count()>0) {
                        $nextdraw = $currentdraw->toArray()[0]['draw'] + 1;
                        $predictrow = $this->Btc->find('all')->where(['draw<='.$nextdraw])->orderDesc('draw')->limit($size);
                        if ($predictrow->count() > 0) {
                            $newdata = $predictrow->toArray();
                            for ($i=0; $i < sizeof($predictrow->toArray()); $i++) {
                                $newdata[$i]['size'] = 0;
                                if($predictrow->toArray()[$i]['result'] >=14){
                                    $newdata[$i]['size'] = 1;
                                }
                                $newdata[$i]['odd'] = $predictrow->toArray()[$i]['result']%2;
                            }
                            $returnmsg = json_encode(["Status" => "00", "Msg" => "Approved", "Data" => $newdata], true);
                        }
                    }
                }
                $time = new Time($result->toArray()[0]['expire']);
                if($time->isPast()) {
                    $returnmsg = json_encode(["Status"=>"03", "Msg"=>"API Key Has Expired"],true);
                }
            } else {
                $returnmsg = json_encode(["Status"=>"02", "Msg"=>"API Key is incorrect"],true);
            }
        }

        $this->response->body($returnmsg);
        $this->response->statusCode(200);
        $this->response->type("application/json");
    }

    /**
     * History method
     *
     * @param string|null $key Keymgr's id.
     * @return \Cake\Http\Response|null
     */
    public function history($key=null)
    {
        $this->autoLayout = false;
        $this->render(false);
        $size=100;
        switch (sizeof($this->request['pass'])) {
            case 1:
                $key = $this->request['pass'][0];
                break;
            case 2:
                $size = $key = $this->request['pass'][0];
                $key = $this->request['pass'][1];
                break;
        }
        if($size>300){
            $size=300;
        }

        if($key!=null){
            $result=TableRegistry::getTableLocator()->get('keymgr')->find('all',['conditions' => ['datakey' => $key]]);
            if($result->count()>0){
                if($result->toArray()[0]['28btc']==0){
                    $returnmsg = json_encode(["Status"=>"04", "Msg"=>"API Key privileges error"],true);
                } else {
                    // 加载历史数据，MYSQL转JSON输出
                    $data = $this->Btc->find('all')->whereNotNull('result')->orderDesc('draw')->limit($size);
                    $newone = $data->toArray();
                    for ($i=0; $i < sizeof($data->toArray()); $i++) {
                        $newone[$i]['size']=0;
                        if($data->toArray()[$i]['result'] >= 14){
                            $newone[$i]['size']=1;
                        }
                        $newone[$i]['odd']=$data->toArray()[$i]['result']%2;
                    }
                    $returnmsg = json_encode(["Status"=>"00","Msg"=>"Approved","Data"=>$newone],true);
                }
                $time = new Time($result->toArray()[0]['expire']);
                if($time->isPast() == true) {
                    $returnmsg = json_encode(["Status"=>"03", "Msg"=>"API Key Has Expired"],true);
                }
            } else {
                $returnmsg = json_encode(["Status"=>"02", "Msg"=>"API Key is incorrect"],true);
            }
        } else {
            $returnmsg = json_encode(["Status"=>"01","Msg"=>"Missing API Key"],true);
        }

        $this->response->body($returnmsg);
        $this->response->statusCode(200);
        $this->response->type("application/json");
    }

    /**
     * Latest method
     *
     * @return \Cake\Http\Response|null
     */
    public function latest(){
        $this->autoLayout = false;
        $this->render(false);

        $data = $this->Btc->find('all')->whereNotNull('result')->orderDesc('draw')->limit(1);
        if($data->count()>0) {
            $odd = $data->toArray()[0]['result']%2;
            $size =0;
            if ( $data->toArray()[0]['result']>13){
                $size=1;
            }

            $returnmsg = json_encode([
                "Status" => "00", "Msg" => "Approved",
                "Draw"=>$data->toArray()[0]['draw'],
                "Time"=>$data->toArray()[0]['time']->i18nFormat("yyyy-MM-dd HH:mm:ss"),'calc'=>$data->toArray()[0]['calc'],
                "result"=>$data->toArray()[0]['result'],
                "Odd"=>$odd,"Size"=>$size], true);
        }
        $this->response->body($returnmsg);
        $this->response->statusCode(200);
        $this->response->type("application/json");
    }
}
