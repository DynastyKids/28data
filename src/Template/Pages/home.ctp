<?php
$this->layout=false;
?>
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<html>
<head>
    <title>28 Data Distribute</title>
</head>
<body>
<h1>28data Testing page</h1>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <h2>28 Canada</h2>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">draw #</th>
                    <th scope="col">time</th>
                    <th scope="col">calc</th>
                    <th scope="col">result</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($cadata as $data){?>
                <tr>
                        <th scope="row"><?= $data->draw?></th>
                        <td><?= $data->time?></td>
                        <td><?= $data->calc?></td>
                        <td><?= $data->result?></td>
                </tr>
                <?php }?>
                </tbody>
            </table>
        </div>
        <div class="col-lg">
            <h2>28 Taiwan</h2>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">draw #</th>
                    <th scope="col">time</th>
                    <th scope="col">calc</th>
                    <th scope="col">result</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($bgdata as $data){?>
                    <tr>
                        <th scope="row"><?= $data->draw?></th>
                        <td><?= $data->time?></td>
                        <td><?= $data->calc?></td>
                        <td><?= $data->result?></td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
        </div>
        <div class="col-lg">
            <h2>28 BTC 1mins</h2>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">draw #</th>
                    <th scope="col">time</th>
                    <th scope="col">calc</th>
                    <th scope="col">result</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($btcdata as $data){?>
                    <tr>
                        <th scope="row"><?= $data->draw?></th>
                        <td><?= $data->time?></td>
                        <td><?= $data->calc?></td>
                        <td><?= $data->result?></td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>
