# 28系列站点数据源

底层页面搭建基于 [CakePHP](https://cakephp.org) 3.9, Skeleton源代码在 [cakephp/cakephp](https://github.com/cakephp/cakephp).

## 安装 & 依赖

1. 使用 [Composer](https://getcomposer.org/doc/00-intro.md) ，如果已有Composer，可以考虑 `composer self-update`.

2. 安装 `php73 mysql php-intl php-mbstring php-xml php-xmlrpc php-mysql`.

3. 需要执行测试服可以使用:
```bash
bin/cake server -p 9876
```
然后访问 `http://localhost:9876` 即可.


## Configuration

Read and edit `config/app.php` and setup the `'Datasources'` and any other
configuration relevant for your application.

## Layout

The app skeleton uses a subset of [Foundation](http://foundation.zurb.com/) (v5) CSS
framework by default. You can, however, replace it with any other library or
custom styles.
